<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>FOCCADI - ONGD</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- estilos generales -->
<link href="css/estilos.css" rel="stylesheet">
<!-- favicon principal -->
<link rel="shortcut icon" href="images/iconos/favicon.ico">
<!-- font-awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- camera-wrap -->
<link href="plugins/camera/camera.css" rel="stylesheet" type="text/css">
<!-- slick slider -->
<link href="plugins/slick/slick.css" rel="stylesheet" type="text/css">
<!--datepicker-->
<link rel="stylesheet" href="plugins/datepicker/bootstrap-datepicker.min.css">

</head>
<body>

<?php
  include 'includes/menu_top.php';
?>






  <div class="container">
    <div id="corrector_altura"></div>
    <div class="row well-lg">
      <div class="col-sm-12 text-center">
        <h2 class="texto_verde_2"><strong>NOSOTROS</strong></h2>
        <br>
      </div>
    </div>
  </div>
  
  <div class="container">
    <div class="row padding_bottom_0">
      <img src="images/interiores/002_FOCCADI_interiores.jpg" class="imagenes_interiores">
    </div>
  </div> 
  <div class="container">
    <div class="row well-lg fondo_general_blanco padding_bottom_0">
      <div class="col-sm-12">
        <h3 class="texto_verde_1 well-sm">¿Quiénes somos?</h3>
        <p class="well-sm">Una Asociación Civil de Derecho Privado sin fines de lucro sujeta a sus Estatutos y al Código Civil, elaboramos proyectos sociales de desarrollo auto sostenibles para la población vulnerable tomando en cuenta sus aptitudes, mediante capacitación académica les otorgamos competencias, damos apoyo para su inserción laboral, y las asociamos para defensa de sus derechos e intercambio de experiencias para una mayor eficiencia en su labor.</p>
        <br><hr>
      </div>
    </div>
    <div class="row well-lg fondo_general_blanco padding_bottom_0">
      <div class="col-sm-6">
        <h3 class="texto_verde_1 well-sm">Misión</h3>
        <p class="well-sm">Contribuir al progreso social y calidad de vida de la persona con propuestas realistas y sostenibles, en coherencia  con sus aptitudes, planes de desarrollo del Gobierno Central y prioridades de fuentes cooperantes.</p>
      </div>
      <div class="col-sm-6">
        <h3 class="texto_verde_1 well-sm">Visión</h3>
        <p class="well-sm">Ser una asociación civil de excelencia, reconocida por otorgar competencias a las personas mediante capacitación académica, difundir conocimientos como línea de base para impulsar a nivel nacional esfuerzos en beneficio de la población vulnerable, transfiriendo técnicas especializadas y actualizadas como cuota de solidaridad en pro del desarrollo social.</p>
      </div>
    </div>
    <div class="row well-lg fondo_general_blanco padding_bottom_0">
      <div class="col-sm-12">
        <hr><br>
        <h3 class="texto_verde_1 well-sm">Formalidad</h3>
        <p class="well-sm">Contamos con registro en las instituciones siguientes:</p>
        <ul class="well-sm padding_left_25">
          <li>SUNARP (Superintendencia Nacional de los Registros Públicos), Partida Registral Nª 11686315</li>
          <li>SUNAT (Superintendencia Nacional de Administración Tributaria):
            <ul>
              <li>Con RUC (Registro Único de Contribuyente), Nº 20511508721</li>
              <li>Como entidad exonerada del Impuesto a la Renta, según Resolución de Intendencia Nº 0490050013578 del 18.07.2006</li>
              <li>Como entidad perceptora de donaciones, según Resolución de Intendencia Nº 0230050197064 del 07.06.2017</li>
            </ul>
          </li>
          <li>APCI (Agencia Peruana de Cooperación Internacional), como Organización No Gubernamental de Desarrollo receptora de Cooperación Técnica Internacional, según Resolución Directoral Nº 146-2016/APCI-DOC del 28.03.2016.</li>
        </ul>
        <br><hr>
      </div>
    </div>
    <div class="row well-lg fondo_general_blanco padding_bottom_0">
      <div class="col-sm-12">      
        <h3 class="texto_verde_1 well-sm">Objetivos</h3>
        <ul class="well-sm padding_left_25">
          <li>Promover y ejecutar programas, proyectos y actividades de desarrollo socio-económico no asistencialistas, en el campo laboral, educación y salud, entre otros.</li>
          <li>Otorgar competencias mediante capacitación académica a las personas para reducir su nivel de pobreza y mejorar su calidad de vida.</li>
          <li>Promover el desarrollo social fortaleciendo la aptitud de la persona para transformarla en una capacidad distintiva.</li>
        </ul>
        <br><hr>
      </div>
    </div>
    <div class="row well-lg fondo_general_blanco">
      <div class="col-sm-3 text-center">
        <p class="well-sm"><br><img src="images/principales/logo_FOCCADI.png" class=""></p>
      </div>
      <div class="col-sm-9">
        <h3 class="texto_verde_1 well-sm">Nuestra identidad</h3>
        <p class="well-sm">Nos identifica nuestro logotipo representado por dos manos teniendo en el centro la palabra FOCCADI, imagen que expresa el alcance de los principios corporativos como cimiento de nuestra organización en apoyo de la sociedad, mediante el FORTALECIMIENTO CREATIVO DE CAPACIDADES DISTINTIVAS en el ser humano, primando el buen trato, cordialidad y respeto a la dignidad de la persona.<br><br></p>
      </div>
    </div>
    <div class="row well-lg fondo_general_blanco padding_bottom_0">
      <div class="col-sm-12">
        <hr><br>
        <h3 class="texto_verde_1 well-sm">Beneficiarios</h3>
        <p class="well-sm">La población en general, con énfasis en personas en condición de vulnerabilidad, discapacitados, adultos mayores, pobres, desocupados, semiempleados y estudiantes de estudios superiores.</p>
        <br><hr>
      </div>
    </div>
  </div>


<br><br><br>

<?php
  include 'includes/footer.php';
?>




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<!-- <script src="js/jquery-1.11.2.min.js"></script> -->
<script src="js/jquery-2.1.1.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.js"></script>

<!-- Galería Camera Wrap -->
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="plugins/camera/camera.min.js"></script>

<!-- Galería Slick Slider -->
<script type="text/javascript" src="plugins/slick/slick.min.js"></script>

<!-- Plugin Datepicker -->
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.min.js"></script>
<script>
  $('.datepicker').datepicker();
</script>

<!-- Plugin Facebook --> 
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10&appId=1057542310947424';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
  $(document).ready(function(){
    
    $("#boton_menu").click(function() {
      if ($("#navegador_bottom_2").is(":visible")) {
        $("#navegador_bottom_2").slideUp("slow");
      } else {
        $("#navegador_bottom_2").slideDown("slow");
      }
    });

    $(window).scroll(function() {
      if ($(document).scrollTop() > 0) {
        $("#navegador_principal").addClass("activado");
        $("#corrector_altura").addClass("activado");
        $("#navegador_1").addClass("activado");
        $(".fondo_verde_2").addClass("activado");
        $("#contenedor_logo").addClass("activado");
        $(".logo_principal").addClass("activado");
        $(".menu_siguenos").addClass("activado"); 
        $("#navegador_top li a").addClass("activado");
        $("#navegador_bottom").addClass("activado"); 
        $("#navegador_bottom li .enlace_bottom").addClass("activado"); 
        $("img.icono_donar").addClass("activado"); 
      } else {
        $("#navegador_principal").removeClass("activado");
        $("#corrector_altura").removeClass("activado");
        $("#navegador_1").removeClass("activado");
        $(".fondo_verde_2").removeClass("activado");
        $("#contenedor_logo").removeClass("activado");
        $(".logo_principal").removeClass("activado");
        $(".menu_siguenos").removeClass("activado");   
        $("#navegador_top li a").removeClass("activado");
        $("#navegador_bottom").removeClass("activado");
        $("#navegador_bottom li .enlace_bottom").removeClass("activado"); 
        $("img.icono_donar").removeClass("activado"); 
      }
    });

  });
</script>




</body>
</html>