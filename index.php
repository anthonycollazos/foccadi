<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>FOCCADI - ONGD</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- estilos generales -->
<link href="css/estilos.css" rel="stylesheet">
<!-- favicon principal -->
<link rel="shortcut icon" href="images/iconos/favicon.ico">
<!-- font-awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- camera-wrap -->
<link href="plugins/camera/camera.css" rel="stylesheet" type="text/css">
<!-- slick slider -->
<link href="plugins/slick/slick.css" rel="stylesheet" type="text/css">
<!--datepicker-->
<link rel="stylesheet" href="plugins/datepicker/bootstrap-datepicker.min.css">

</head>
<body>

<?php
  include 'includes/menu_top.php';
?>




  <div class="container">
    <div id="corrector_altura"></div>
    <div class="row">
      <section class="camera_wrap camera_white_skin visibilidad-pc" id="camera_wrap_1">
        <div data-thumb="images/slider/01_slider_FOCCADI_mini.jpg" data-src="images/slider/01_slider_FOCCADI.jpg">
          <div class="camera_caption fadeFromRight">
            <div class="transparencia"></div>
            <a class="btn" href="#" target="_blank">Proyecto SER Cap-AZ:<br><span style="font-size: 14px; line-height: 14px;">Persona Capacitada en el soporte técnico del adulto Mayor con enfermedad de<br>Alzheimer y otras demencias</span></a>
          </div>
        </div>
        <div data-thumb="images/slider/02_slider_FOCCADI_mini.jpg" data-src="images/slider/02_slider_FOCCADI.jpg">
          <div class="camera_caption fadeFromLeft">
            <div class="transparencia"></div>
            <a class="btn" href="#" target="_blank">ONGD FOCCADI<br><span>"FORTALECIMIENTO CREATIVO DE CAPACIDADES DISTINTIVAS"</span></a>
          </div>
        </div>
        <div data-thumb="images/slider/03_slider_FOCCADI_mini.jpg" data-src="images/slider/03_slider_FOCCADI.jpg">
          <div class="camera_caption fadeFromLeft">
            <div class="transparencia"></div>
            <a class="btn" href="#" target="_blank">ONGD FOCCADI<br><span>"FORTALECIMIENTO CREATIVO DE CAPACIDADES DISTINTIVAS"</span></a>
          </div>
        </div>
      </section><!--cierra camera_wrap -->
    </div>
    <div class="row">
      <img src="images/principales/sombra_slider_FOCCADI.png" class="img-responsive" alt="">
    </div>
  </div>

  <div class="reparacion_altura_1">
    <div id="quienes_somos" class="reparacion_altura_2"></div>
  </div>
  <div class="container">
    <div class="row well-lg">      
      <div class="col-sm-12 text-center">
        <h2 class="texto_verde_2"><strong>¿QUIÉNES SOMOS?</strong></h2>
        <br>
        <h4 class="texto_verde_1 fondo_general_blanco well-sm"><strong>ONGD "FORTALECIMIENTO CREATIVO DE CAPACIDADES DISTINTIVAS" - FOCCADI</strong></h4>
        <p>Una Asociación Civil de Derecho Privado sin fines de lucro sujeta a sus Estatutos y al Código Civil, elaboramos proyectos sociales de desarrollo auto sostenibles para la población vulnerable tomando en cuenta sus aptitudes, mediante capacitación académica les otorgamos competencias, damos apoyo para su inserción laboral, y las asociamos para defensa de sus derechos e intercambio de experiencias para una mayor eficiencia en su labor.</p>
        <br>
        <button type="button" class="btn btn-foccadi btn-sm" onclick="window.location.href='nosotros.php'"><i class="fa fa-plus-circle texto_blanco margin_right_10" aria-hidden="true"></i><strong>Más Información...</strong></button>
        <br><br>
      </div>      
    </div>
  </div>  

  <div class="reparacion_altura_1">
    <div id="que_hacemos" class="reparacion_altura_2"></div>
  </div>
  <div class="container">
    <div class="row well-lg fondo_general_blanco">
      <br><br>      
      <div class="col-sm-4 text-center">
        <img src="images/iconos/icono_01a_FOCCADI.png" class="img-responsive icono_principal" alt="">
        <br>
        <h3 class="texto_verde_1 well-sm"><strong>¿QUÉ HACEMOS?</strong></h3>
        <p class="well-sm"><strong>A)</strong> Promover y ejecutar programas, proyectos y actividades de desarrollo socio-económico no asistencialistas, en el campo laboral, educación y salud, entre otros.<br><br><strong>B)</strong> Otorgar competencias mediante capacitación académica a las personas para reducir su nivel de pobreza y mejorar su calidad de vida.<br><strong>C)</strong> Promover el desarrollo social fortaleciendo la aptitud de la persona para transformarla en una capacidad distintiva.</p>
        <br><br>
      </div>
      <div class="col-sm-4 text-center">
        <img src="images/iconos/icono_03a_FOCCADI.png" class="img-responsive icono_principal" alt="">
        <br>
        <h3 class="texto_verde_1 well-sm"><strong>¿QUÉ PUEDES HACER TU?</strong></h3>
        <p class="well-sm">Ayudarnos a ayudar a través de:<br><br>
        <strong>A)</strong> Donación pecuniaria, depositando tu aporte en nuestra:<br>
        - Cuenta Corriente Soles Nº 637-300133522-4<br>
        - Banco Interbank<br>
        - Código Interbancario Nº 003-637-003001335224-67<br>
        - Titular: O.N.G.D. Foccadi<br><br>
        <strong>B)</strong> Uniéndote a nuestro equipo
        Próximamente iniciaremos un programa de voluntariado, para que compartas tus conocimientos, tu formación y tu experiencia, en apoyo de nuestras intervenciones. 
        </p>
        <br><br>
      </div>
      <div class="col-sm-4 text-center">
        <img src="images/iconos/icono_05a_FOCCADI.png" class="img-responsive icono_principal" alt="">
        <br>
        <h3 class="texto_verde_1 well-sm"><strong>¿CÓMO RETRIBUIMOS TU APOYO?</strong></h3>
        <p class="well-sm"><strong>A)</strong> Otorgándole un “Certificado de Donación”, el cual le permitirá deducir su aporte  hasta por el 10% del monto de su Renta Neta como pago a cuenta del Impuesto correspondiente. Acción reconocida por la SUNAT al estar la O.N.G.D. FOCCADI calificada como entidad perceptora de donaciones según Resolución de Intendencia Nº 0230050197064 del 07.06.2017 e inscrita en el “Registro de Entidades Perceptoras de Donaciones”.<br><br><strong>B)</strong> Difundiendo su acción de responsabilidad social en la comunidad por medio de nuestra página WEB y en conferencias que realizaremos a nivel nacional.</p>
        <br><br>
      </div>
    </div>
  </div>




  

<?php
//conexion a la base de datos de noticias
require "admin/config.php";
//envio de la instruccion sql
$sql = "SELECT * FROM noticias ORDER BY noticia_id DESC LIMIT 4";
$resultado = $conexion->query($sql);
?>

  <div class="container">
    <div class="row well-lg">
      <div class="col-sm-12 text-center">
        <br>
        <h2 class="texto_verde_2"><strong>NOTICIAS</strong></h2>
        <br>
      </div>
    </div>  
    <div class="row">
      

      <?php

      if ($resultado->num_rows > 0) {
          for ($i=0; $i<2; $i++) {
                $row = $resultado->fetch_assoc();
                $contenido = $row["descripcion"];
                $fecha = $row["fecha_post_ini"];
                $fechaSeparada = explode('-', $fecha);
                $fechaOrdenada = $fechaSeparada[2] . "/" . $fechaSeparada[1] . "/" . $fechaSeparada[0];
                $contenidoMaximo = substr($contenido, 0, 200);
                $noticiaId = $row["noticia_id"];
                $nombreCategoria = strtolower($row["nombre_categoria"]);
                echo "<div class='col-md-6'>
                    <div class='noticia_caja margin_bottom_20'>
                      <div class='noticia_foto'>
                        <div class='noticia_img' style='background-image: url(admin/images/" . $row["imagen"] . ");'></div>
                      </div>
                      <div class='noticia_contenido'>
                        <h4 class='texto_verde_1'><strong>" . $row["titulo"] . "</strong></h4>
                        <hr>
                        <p class='noticia_texto'>" . $contenidoMaximo . "...</p>
                        <a href='" . $nombreCategoria . ".php?id=" . $noticiaId . "' class='noticia_enlace'><i class='fa fa-plus-circle texto_verde_1 margin_right_5' aria-hidden='true'></i>Leer más...</a>
                        <p class='noticia_subtexto'><br>Posteado el " . $fechaOrdenada . "</p>
                      </div>
                      <div class='clearfix'></div>
                    </div>
                  </div>";
          }
      } else {
          echo "0 resultados";
      }

      ?>

  
    </div>
    <div class="row">
      

      <?php

      if ($resultado->num_rows > 0) {
          for ($i=2; $i<4; $i++) {
                $row = $resultado->fetch_assoc();
                $contenido = $row["descripcion"];
                $fecha = $row["fecha_post_ini"];
                $fechaSeparada = explode('-', $fecha);
                $fechaOrdenada = $fechaSeparada[2] . "/" . $fechaSeparada[1] . "/" . $fechaSeparada[0];
                $contenidoMaximo = substr($contenido, 0, 200);
                $noticiaId = $row["noticia_id"];
                $nombreCategoria = strtolower($row["nombre_categoria"]);
                echo "<div class='col-md-6'>
                    <div class='noticia_caja margin_bottom_20'>
                      <div class='noticia_foto'>
                        <div class='noticia_img' style='background-image: url(admin/images/" . $row["imagen"] . ");'></div>
                      </div>
                      <div class='noticia_contenido'>
                        <h4 class='texto_verde_1'><strong>" . $row["titulo"] . "</strong></h4>
                        <hr>
                        <p class='noticia_texto'>" . $contenidoMaximo . "...</p>
                        <a href='" . $nombreCategoria . ".php?id=" . $noticiaId . "' class='noticia_enlace'><i class='fa fa-plus-circle texto_verde_1 margin_right_5' aria-hidden='true'></i>Leer más...</a>
                        <p class='noticia_subtexto'><br>Posteado el " . $fechaOrdenada . "</p>
                      </div>
                      <div class='clearfix'></div>
                    </div>
                  </div>";
          }
      } else {
          echo "0 resultados";
      }

      ?>

  
    </div>
  </div>

<?php
  $conexion->close();
?>


      


  <div class="container">
    <div class="row well-lg">
      <div class="col-md-12">
        <br>


<?php

    //$number = 6;
    //include("C:\\wamp64\\www\\foccadi\\plugins\\cutenews\\show_news.php");

?>



        <br>
      </div>
    </div>
  </div>





  <div class="container">
    <div class="row well-lg">
      <div class="col-md-12 text-center">
        <br><br>
        <h2 class="texto_verde_1"><strong>SÍGUENOS...</strong></h2>
      </div>
    </div>
    <div class="row well-lg fondo_general_gris">
      <div class="col-md-6 text-center">
        <br>
          <a class="twitter-grid" data-limit="3" href="https://twitter.com/TwitterDev/timelines/539487832448843776?ref_src=twsrc%5Etfw">National Park Tweets</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
          <!--  <a class="twitter-timeline" data-tweet-limit="3" href="https://twitter.com/TwitterDev/timelines/539487832448843776?ref_src=twsrc%5Etfw">National Park Tweets - Curated tweets by TwitterDev</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> -->
        <br>
      </div>
      <div class="col-md-6 text-center">
        <br>
          <iframe width="100%" height="400" src="https://www.youtube.com/embed/gkoTcm-SIWo" frameborder="0" allowfullscreen></iframe>
        <br><br>
        <div class="fb-page"
          data-href="https://www.facebook.com/Caramelover-1003072149821247/"
          data-height="562"
          data-tabs="timeline,events,messages"
          data-small-header="false"
          data-adapt-container-width="true"
          data-hide-cover="false"
          data-show-facepile="false">
            <blockquote cite="https://www.facebook.com/Caramelover-1003072149821247/" class="fb-xfbml-parse-ignore">
              <a href="https://www.facebook.com/Caramelover-1003072149821247/">Caramelover</a>
            </blockquote>
        </div>
        <br>
      </div>
    </div>
  </div>

<br><br><br>














<!-- 

        <section class="barra-socios">
            <div class="slick-slider">
                <div class="slick-slide">
                    <a href="http://www.smarkia.com/es" target="_blank"><img src="images/socios/socios1.jpg" class="img_size"></a>
                </div>
                <div class="slick-slide">
                    <a href="http://www.kromschroeder.es/" target="_blank"><img src="images/socios/socios3.jpg" class="img_size"></a>
                </div>
                <div class="slick-slide">
                    <a href="http://www.emh-metering.com/" target="_blank"><img src="images/socios/socios4.jpg" class="img_size"></a>
                </div>
                <div class="slick-slide">
                    <a href="http://www.advanticsys.com/" target="_blank"><img src="images/socios/socios5.jpg" class="img_size"></a>
                </div>
            </div>
        </section>





<div class="container-fluid">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <h1 class="text-center">Bootstrap with Dreamweaver</h1>
    </div>
  </div>
  <hr>
</div>
<div class="container">
  <div class="row text-center">
    <div class="col-md-6 col-md-offset-3">Click outside the blue container to select this <strong>row</strong>. Columns are always contained within a row. <strong>Rows are indicated by a dashed grey line and rounded corners</strong>. </div>
  </div>
  <hr>
  <div class="row">
    <div class="text-justify col-sm-4"> Click here to select this<strong> column.</strong> Always place your content within a column. Columns are indicated by a dashed blue line. </div>
    <div class="col-sm-4 text-justify"> You can <strong>resize a column</strong> using the handle on the right. Drag it to increase or reduce the number of columns.</div>
    <div class="col-sm-4 text-justify"> You can <strong>offset a column</strong> using the handle on the left. Drag it to increase or reduce the offset. </div>
  </div>
  <hr>
  <div class="row">
    <div class="text-center col-md-12">
      <div class="well"><strong> Easily build your page using the Bootstrap components from the Insert panel.</strong></div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4 text-center">
      <h4>Adding <strong>Buttons</strong></h4>
      <p>Quickly add buttons to your page by using the button component in the insert panel. </p>
      <button type="button" class="btn btn-info btn-sm">Info Button</button>
      <button type="button" class="btn btn-success btn-sm">Success Button</button>
    </div>
    <div class="text-center col-sm-4">
      <h4>Adding <strong>Labels</strong></h4>
      <p>Using the insert panel, add labels to your page by using the label component.</p>
      <span class="label label-warning">Info Label</span><span class="label label-danger">Danger Label</span> </div>
    <div class="text-center col-sm-4">
      <h4>Adding <strong>Glyphicons</strong></h4>
      <p>You can also add glyphicons to your page from within the insert panel.</p>
      <div class="row">
        <div class="col-xs-4"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></div>
        <div class="col-xs-4"><span class="glyphicon glyphicon-home" aria-hidden="true"> </span> </div>
        <div class="col-xs-4"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="text-center col-md-6 col-md-offset-3">
      <h4>Footer </h4>
      <p>Copyright &copy; 2015 &middot; All Rights Reserved &middot; <a href="http://yourwebsite.com/" >My Website</a></p>
    </div>
  </div>
  <hr>
</div> -->

  

<?php
  include 'includes/footer.php';
?>









<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<!-- <script src="js/jquery-1.11.2.min.js"></script> -->
<script src="js/jquery-2.1.1.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.js"></script>

<!-- Galería Camera Wrap -->
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="plugins/camera/camera.min.js"></script>
<script>
  jQuery(function(){
    jQuery('#camera_wrap_1').camera({
      thumbnails: true,
    });
  });
</script>

<!-- Galería Slick Slider -->
<script type="text/javascript" src="plugins/slick/slick.min.js"></script>
<script>
$(document).ready(function(){
  $('.slick-slider').slick({
    autoplay: true,
    autoplaySpeed: 3000,
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1279,
      settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      dots: true
      }
    },
    {
      breakpoint: 799,
      settings: {
      slidesToShow: 2,
      slidesToScroll: 1
      }
    },
    {
      breakpoint: 479,
      settings: {
      slidesToShow: 1,
      slidesToScroll: 1
      }
    }
    ]
  });
});
</script>

<!-- Plugin Datepicker -->
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.min.js"></script>
<script>
  $('.datepicker').datepicker();
</script>

<!-- Plugin Facebook --> 
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10&appId=1057542310947424';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
  $(document).ready(function(){
    
    $("#boton_menu").click(function() {
      if ($("#navegador_bottom_2").is(":visible")) {
        $("#navegador_bottom_2").slideUp("slow");
      } else {
        $("#navegador_bottom_2").slideDown("slow");
      }
    });

    $(window).scroll(function() {
      if ($(document).scrollTop() > 0) {
        $("#navegador_principal").addClass("activado");
        $("#corrector_altura").addClass("activado");
        $("#navegador_1").addClass("activado");
        $(".fondo_verde_2").addClass("activado");
        $("#contenedor_logo").addClass("activado");
        $(".logo_principal").addClass("activado");
        $(".menu_siguenos").addClass("activado"); 
        $("#navegador_top li a").addClass("activado");
        $("#navegador_bottom").addClass("activado"); 
        $("#navegador_bottom li .enlace_bottom").addClass("activado"); 
        $("img.icono_donar").addClass("activado"); 
      } else {
        $("#navegador_principal").removeClass("activado");
        $("#corrector_altura").removeClass("activado");
        $("#navegador_1").removeClass("activado");
        $(".fondo_verde_2").removeClass("activado");
        $("#contenedor_logo").removeClass("activado");
        $(".logo_principal").removeClass("activado");
        $(".menu_siguenos").removeClass("activado");   
        $("#navegador_top li a").removeClass("activado");
        $("#navegador_bottom").removeClass("activado");
        $("#navegador_bottom li .enlace_bottom").removeClass("activado"); 
        $("img.icono_donar").removeClass("activado"); 
      }
    });

  });
</script>




</body>
</html>