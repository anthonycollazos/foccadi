<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>FOCCADI - ONGD</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- estilos generales -->
<link href="css/estilos.css" rel="stylesheet">
<!-- favicon principal -->
<link rel="shortcut icon" href="images/iconos/favicon.ico">
<!-- font-awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- camera-wrap -->
<link href="plugins/camera/camera.css" rel="stylesheet" type="text/css">
<!-- slick slider -->
<link href="plugins/slick/slick.css" rel="stylesheet" type="text/css">
<!--datepicker-->
<link rel="stylesheet" href="plugins/datepicker/bootstrap-datepicker.min.css">

</head>
<body>

<?php
  include 'includes/menu_top.php';
?>






 
<?php
//conexion a la base de datos de noticias
require "admin/config.php";
//envio de la instruccion sql
$sql = "SELECT * FROM noticias WHERE nombre_categoria='Conferencias' ORDER BY noticia_id DESC LIMIT 6";
$resultado = $conexion->query($sql);
?>

  <div class="container">
    <div id="corrector_altura"></div>
    <div class="row well-lg">
      <div class="col-sm-12 text-center">
        <br>
        <h2 class="texto_verde_2"><strong>CONFERENCIAS</strong></h2>
        <br>
      </div>
    </div>  
    <div class="row">
      

      <?php

      if ( isset($_GET['id']) ) {
        $id = $_GET['id'];    
        $sql2 = "SELECT * FROM noticias WHERE noticia_id = '" . $id . "'";
        $resultado2 = $conexion->query($sql2);
        if (!$resultado2) {
          echo 'No se pudo ejecutar la consulta: ' . mysql_error();
          exit;
        }
        $fila = $resultado2->fetch_assoc();

        // MOSTRAR TODO EL CONTENIDO

        $fecha = $fila["fecha_post_ini"];
        $fechaSeparada = explode('-', $fecha);
        $fechaOrdenada = $fechaSeparada[2] . "/" . $fechaSeparada[1] . "/" . $fechaSeparada[0];
        $nombreCategoria = strtolower($fila["nombre_categoria"]);

        echo "<div class='col-sm-12 padding_right_0 padding_left_0 fondo_general_blanco'><div class='row'>
              <div class='col-md-6'>
                <img src='admin/images/" . $fila["imagen"] . "' class='imagenes_interiores'>
              </div>
              <div class='col-md-6'>
                <div class='well-lg'>
                    <h4 class='texto_verde_1 well-sm'><strong>" . $fila["titulo"] . "</strong></h4>
                    <hr>
                    <p class='well-sm'>" . $fila["descripcion"] . "...</p>
                    <p class='noticia_subtexto well-sm'><br>Posteado el " . $fechaOrdenada . "<br></p>
                    <div class='well-sm'>
                      <a href='" . $nombreCategoria . ".php' class='btn btn-foccadi btn-sm'>
                        <i class='fa fa-chevron-circle-left texto_blanco margin_right_10' aria-hidden='true'></i><strong>Regresar...</strong>
                      </a>
                    </div>
                </div>
              </div>
          </div></div>";

      } else {

          if ($resultado->num_rows > 0) {
              for ($i=0; $i<2; $i++) {
                $row = $resultado->fetch_assoc();
                $contenido = $row["descripcion"];
                $fecha = $row["fecha_post_ini"];
                $fechaSeparada = explode('-', $fecha);
                $fechaOrdenada = $fechaSeparada[2] . "/" . $fechaSeparada[1] . "/" . $fechaSeparada[0];
                $contenidoMaximo = substr($contenido, 0, 200);
                $noticiaId = $row["noticia_id"];
                $nombreCategoria = strtolower($row["nombre_categoria"]);
                echo "<div class='col-md-6'>
                    <div class='noticia_caja margin_bottom_20'>
                      <div class='noticia_foto'>
                        <div class='noticia_img' style='background-image: url(admin/images/" . $row["imagen"] . ");'></div>
                      </div>
                      <div class='noticia_contenido'>
                        <h4 class='texto_verde_1'><strong>" . $row["titulo"] . "</strong></h4>
                        <hr>
                        <p class='noticia_texto'>" . $contenidoMaximo . "...</p>
                        <a href='" . $nombreCategoria . ".php?id=" . $noticiaId . "' class='noticia_enlace'><i class='fa fa-plus-circle texto_verde_1 margin_right_5' aria-hidden='true'></i>Leer más...</a>
                        <p class='noticia_subtexto'><br>Posteado el " . $fechaOrdenada . "</p>
                      </div>
                      <div class='clearfix'></div>
                    </div>
                  </div>";
              }
          } else {
              echo "0 resultados";
          }
  
          echo '</div>';
          echo '<div class="row">';
          
          if ($resultado->num_rows > 0) {
              for ($i=2; $i<4; $i++) {
                $row = $resultado->fetch_assoc();
                $contenido = $row["descripcion"];
                $fecha = $row["fecha_post_ini"];
                $fechaSeparada = explode('-', $fecha);
                $fechaOrdenada = $fechaSeparada[2] . "/" . $fechaSeparada[1] . "/" . $fechaSeparada[0];
                $contenidoMaximo = substr($contenido, 0, 200);
                $noticiaId = $row["noticia_id"];
                $nombreCategoria = strtolower($row["nombre_categoria"]);
                echo "<div class='col-md-6'>
                    <div class='noticia_caja margin_bottom_20'>
                      <div class='noticia_foto'>
                        <div class='noticia_img' style='background-image: url(admin/images/" . $row["imagen"] . ");'></div>
                      </div>
                      <div class='noticia_contenido'>
                        <h4 class='texto_verde_1'><strong>" . $row["titulo"] . "</strong></h4>
                        <hr>
                        <p class='noticia_texto'>" . $contenidoMaximo . "...</p>
                        <a href='" . $nombreCategoria . ".php?id=" . $noticiaId . "' class='noticia_enlace'><i class='fa fa-plus-circle texto_verde_1 margin_right_5' aria-hidden='true'></i>Leer más...</a>
                        <p class='noticia_subtexto'><br>Posteado el " . $fechaOrdenada . "</p>
                      </div>
                      <div class='clearfix'></div>
                    </div>
                  </div>";
              }
          } else {
              echo "0 resultados";
          }

          echo '</div>';
          echo '<div class="row">';

          if ($resultado->num_rows > 0) {
              for ($i=4; $i<6; $i++) {
                $row = $resultado->fetch_assoc();
                $contenido = $row["descripcion"];
                $fecha = $row["fecha_post_ini"];
                $fechaSeparada = explode('-', $fecha);
                $fechaOrdenada = $fechaSeparada[2] . "/" . $fechaSeparada[1] . "/" . $fechaSeparada[0];
                $contenidoMaximo = substr($contenido, 0, 200);
                $noticiaId = $row["noticia_id"];
                $nombreCategoria = strtolower($row["nombre_categoria"]);
                echo "<div class='col-md-6'>
                    <div class='noticia_caja margin_bottom_20'>
                      <div class='noticia_foto'>
                        <div class='noticia_img' style='background-image: url(admin/images/" . $row["imagen"] . ");'></div>
                      </div>
                      <div class='noticia_contenido'>
                        <h4 class='texto_verde_1'><strong>" . $row["titulo"] . "</strong></h4>
                        <hr>
                        <p class='noticia_texto'>" . $contenidoMaximo . "...</p>
                        <a href='" . $nombreCategoria . ".php?id=" . $noticiaId . "' class='noticia_enlace'><i class='fa fa-plus-circle texto_verde_1 margin_right_5' aria-hidden='true'></i>Leer más...</a>
                        <p class='noticia_subtexto'><br>Posteado el " . $fechaOrdenada . "</p>
                      </div>
                      <div class='clearfix'></div>
                    </div>
                  </div>";
              }
          } else {
              echo "0 resultados";
          }

      }

    ?>

  
    </div> 
  </div>

<?php
  $conexion->close();
?>



<br><br><br>

<?php
  include 'includes/footer.php';
?>




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<!-- <script src="js/jquery-1.11.2.min.js"></script> -->
<script src="js/jquery-2.1.1.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.js"></script>

<!-- Galería Camera Wrap -->
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="plugins/camera/camera.min.js"></script>

<!-- Galería Slick Slider -->
<script type="text/javascript" src="plugins/slick/slick.min.js"></script>

<!-- Plugin Datepicker -->
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.min.js"></script>
<script>
  $('.datepicker').datepicker();
</script>

<!-- Plugin Facebook --> 
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10&appId=1057542310947424';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
  $(document).ready(function(){
    
    $("#boton_menu").click(function() {
      if ($("#navegador_bottom_2").is(":visible")) {
        $("#navegador_bottom_2").slideUp("slow");
      } else {
        $("#navegador_bottom_2").slideDown("slow");
      }
    });

    $(window).scroll(function() {
      if ($(document).scrollTop() > 0) {
        $("#navegador_principal").addClass("activado");
        $("#corrector_altura").addClass("activado");
        $("#navegador_1").addClass("activado");
        $(".fondo_verde_2").addClass("activado");
        $("#contenedor_logo").addClass("activado");
        $(".logo_principal").addClass("activado");
        $(".menu_siguenos").addClass("activado"); 
        $("#navegador_top li a").addClass("activado");
        $("#navegador_bottom").addClass("activado"); 
        $("#navegador_bottom li .enlace_bottom").addClass("activado"); 
        $("img.icono_donar").addClass("activado"); 
      } else {
        $("#navegador_principal").removeClass("activado");
        $("#corrector_altura").removeClass("activado");
        $("#navegador_1").removeClass("activado");
        $(".fondo_verde_2").removeClass("activado");
        $("#contenedor_logo").removeClass("activado");
        $(".logo_principal").removeClass("activado");
        $(".menu_siguenos").removeClass("activado");   
        $("#navegador_top li a").removeClass("activado");
        $("#navegador_bottom").removeClass("activado");
        $("#navegador_bottom li .enlace_bottom").removeClass("activado"); 
        $("img.icono_donar").removeClass("activado"); 
      }
    });

  });
</script>




</body>
</html>