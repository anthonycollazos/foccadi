<?php
session_start();
//si hay una sesión
if (isset($_SESSION['name'])){
//se muestra el contenido de la página web

//incluimos la conexion 
require "config.php";
//iniciamos html
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php require "head.php"; ?>
<title>NOTICIAS | FOCCADI - ONGD</title>
</head>
<body>
<nav class="navbar navbar-default modal_estilo_nav">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><img src="../images/principales/logo_FOCCADI_texto.png" class="logo_FOCCADI_texto"></a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-user texto_blanco margin_right_10" aria-hidden="true"></i>
              <?php print $_SESSION['name'];?> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                  <a href="cerrar_sesion.php" class="cerrar_sesion"><i class="fa fa-times-circle texto_blanco margin_right_10" aria-hidden="true"></i>Salir</a>
                </li>
            </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  <div class="row well-lg fondo_general_blanco">
    <div class="col-sm-4 col-xs-4">
      <h2 class="texto_verde_1 margin_top_0 margin_bottom_0">Eliminar</h2>
    </div>    
    <div class="col-sm-8 col-xs-8 text-right">      
      <button type="button" class="btn btn-primary icono_noticia_adm" onclick="window.location.href='inicio.php'">
        <i class="fa fa-home texto_blanco" aria-hidden="true"></i><span class="margin_left_10">Inicio</span>
      </button>
      <button type="button" class="btn btn-info icono_noticia_adm" onclick="window.location.href='ingresar_noticia.php'">
        <i class="fa fa-newspaper-o texto_blanco" aria-hidden="true"></i><span class="margin_left_10">Ingresar Noticias</span>
      </button>
      <button type="button" class="btn btn-warning icono_noticia_adm" onclick="window.location.href='eliminar_noticia.php'">
        <i class="fa fa-trash texto_blanco" aria-hidden="true"></i><span class="margin_left_10">Eliminar Noticias</span>
      </button>  
    </div>
  </div>
  <hr class="margin_top_0 margin_bottom_0">
  <div class="row well-lg fondo_general_blanco">
    <div class="col-sm-12">

        <!-- escondemos el mensaje hasta que sea necesario -->
        <div id="delete-ok" style="display:none;"> </div>
<?php
      $instruccion = "select * from noticias order by noticia_id desc";
      //$consulta = mysql_query ($instruccion, $conexion)
      $consulta = mysqli_query($conexion,$instruccion)
         or die ("Consulta de datos errónea...");
      //$nfilas = mysql_num_rows ($consulta);
      $nfilas = mysqli_num_rows($consulta);
      if ($nfilas > 0)
      {
 
         for ($i=0; $i<$nfilas; $i++) {

            $resultado = mysqli_fetch_array($consulta);
            $contenido = $resultado['descripcion'];
            $contenidoMaximo = substr($contenido, 0, 400);

            echo "<div class='row margin_0' id='" . $resultado['noticia_id'] . "' data='" . $resultado['noticia_id'] . "'>
                  <div class='caja_noticia_adm margin_bottom_20'>
                    <div class='col-sm-2'>
                      <a href='" . $resultado['url'] . "' target='_blank' title='" . $resultado['titulo'] . "'>
                        <div class='img_noticia_adm margin_bottom_10' style='background-image: url(images/" . $resultado['imagen'] . ");'></div>
                      </a>
                    </div>
                    <div class='col-sm-10'>
                      <p>Categoría: <strong>" . $resultado['nombre_categoria'] . "</strong> | Publicado: <strong>" . $resultado['fecha_post_ini'] . "</strong></p>
                      <h4 class='texto_verde_1'><strong>
                        <a title='" . $resultado['titulo'] . "' href='" . $resultado['url'] . "' target='_blank'>" . $resultado['titulo'] . "</a>
                      </strong></h4>
                      <p class='noticia_texto'>" . $contenidoMaximo . "...<br></p>
                      <button type='button' class='delete btn btn-danger' id='" . $resultado['noticia_id'] ."'>
                        <i class='fa fa-times-circle texto_blanco margin_right_10' aria-hidden='true'></i>Eliminar
                      </button>
                    </div>
                    <div class='clearfix'></div>
                  </div>                  
                </div>
            ";

/*
  echo "<div class='service_list' id='". $resultado['noticia_id'] ."' data='". $resultado['noticia_id'] ."'>";
    echo "<div class='center_block'>"; 
      echo "<a href='". $resultado['url'] ."' class='product_img_link' target='_blank' title='". $resultado['titulo'] ."'><img width='129' height='129' src='images/". $resultado['imagen'] ."'></a>";
      echo '<span class="info_categoria">Categoria: '. $resultado['nombre_categoria'] .'</span>';
      echo '<h3><a title="'. $resultado['titulo'] .'" href="'. $resultado['url'] .'" target="_blank">'. $resultado['titulo'] .'</a></h3>';
      echo '<p class="product_desc">' . $resultado['descripcion'] . '<br></p>';
    echo"</div>";
    echo "<a class='delete' id='". $resultado['noticia_id'] ."'>Eliminar</a>";
  echo "</div>";
*/


 
        }
 
      }
      else {
        print '<p class="no_noticia">No hay noticias para eliminar...</p><a class="product_desc" href="ingresar_noticia.php">Ingresar noticia nueva</a>';
      }


?>



      <hr class="margin_bottom_0">
      <p class="well-sm margin_bottom_0 text-center">
        <button type="button" class="btn btn-primary btn-sm" onclick="window.location.href='inicio.php'">
          <i class="fa fa-home texto_blanco margin_right_10" aria-hidden="true"></i>Inicio
        </button>
        <button type="button" class="btn btn-info btn-sm" onclick="window.location.href='ingresar_noticia.php'">
          <i class="fa fa-commenting texto_blanco margin_right_10" aria-hidden="true"></i>Ingresar Noticias
        </button>
        <button type="button" class="btn btn-warning btn-sm" onclick="window.location.href='eliminar_noticia.php'">
          <i class="fa fa-trash texto_blanco margin_right_10" aria-hidden="true"></i>Eliminar Noticias
        </button>
      </p>
      <hr class="margin_top_0">
    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery-2.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.js"></script> 
<!-- funciones básicas js -->
<script src="main.js"></script>
<!-- creamos el javascript para la elimnacion -->
<script type="text/javascript">
$(document).ready(function() { 
  $('.delete').click(function(){   
    var parent = $(this).parent().parent().parent().attr('id');
    var noticia = $(this).parent().parent().parent().attr('data');
    var dataString = 'id='+noticia;       
    $.ajax({
      type: "POST",
      url: "delete.php",
      data: dataString,
      success: function() {
        $("#delete-ok").empty();
        $("#delete-ok").append("<h4 class='texto_verde_2 well-sm margin_top_0'>Noticia N° " + noticia + " eliminada correctamente.</h4>").fadeIn("slow");
        $("#" + parent).fadeOut("slow");
      }
    });
  });  
});    
</script>
<script type="text/javascript">
// Función que cierra la pagina en inactividad
  var idleTime = 0;
  $(document).ready(function(){
    var idleInterval = setInterval(timerIncrement, 1000); // segundo
    $(this).mousemove(function(e){
        idleTime = 0;
    });
    $(this).keypress(function(e){
        idleTime = 0;
    });
  });
  function timerIncrement() {
      idleTime = idleTime + 1;
      if (idleTime < 1200) { // 60 segundos = 1 minuto
        //$("#texto2").text("movimiento");        
      } else {
        window.location.replace("cerrar_sesion.php");
        //$("#texto2").text("detenido");
      }
  }
  timerIncrement();
</script>
</body>
</html>

<?php
//si no hay sesión
} else {
    //se redirecciona
    header ('location: index.php');
}
?>