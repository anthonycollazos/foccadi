<?php
session_start();
//si hay una sesión
if (isset($_SESSION['name'])){
//se muestra el contenido de la página web

//incluimos la conexion 
require "config.php";
//iniciamos html
?>
<?php
 
function validateTitulo($title) {    
  if (strlen($title) < 8) {
    //NO cumple longitud minima
    return false;
  /* } elseif (!preg_match("/^[_a-zA-Z0-9- ]+$/", $title)) {
    //SI longitud pero NO cumple acaracteres solo caracteres A-z
    return false; */
  } else {
    return true;
  }
}
function validateNoticia($notice) { 
    if (strlen($notice) < 40) {
      //NO cumple longitud minima
      return false;
    /* } elseif (!preg_match("/^[_a-zA-Z0-9- ]+$/", $notice)) {
      //SI longitud pero NO solo caracteres A-z
      return false; */
    } else {
      return true;
    }
}
function validateEmail($email) {    
    if (strlen($email) == 0) {
      //NO hay nada escrito
      return false;  
    } elseif (!filter_var($_POST['email'], FILTER_SANITIZE_EMAIL)) {
      //esta escrito,pero  NO es VALIDO email
      return false;
    } else {
        return true;
    }
}
//variables valores por defecto
$titulo = "";
$tituloValue = "";
$noticia = "";
$noticiaValue = "";
$email = "";
$emailValue = "";
$urlnoticiaValue = "";
  
if ( isset($_POST['titulo']) ) {
    $insertar = $_REQUEST['insertar'];
    $titulo = $_REQUEST['titulo'];
    $noticia = $_REQUEST['noticia'];
    $categoria = $_REQUEST['categoria'];
    $email = $_REQUEST['email'];
}
 
   $error = false;

//Validacion de datos enviados
  if (isset($insertar)) {
 
    if(!validateTitulo($_POST['titulo'])) {
      $titulo = "error";
    }
    if(!validateNoticia($_POST['noticia'])) {
      $noticia = "errorERROR";
    }
    if(!validateEmail($_POST['email'])) {
      $email = "error";
    }
     
    //Guardamos valores para que no tenga que reescribirlos
    $tituloValue = $_POST['titulo'];
    $noticiaValue = $_POST['noticia'];
    $emailValue = $_POST['email'];
    $urlnoticiaValue = $_POST['urlnoticia'];


/*    echo htmlspecialchars("<a href='test'>Testaaaaaaaaaa</a>", ENT_COMPAT,'ISO-8859-1', true);
    echo htmlentities("<a href='test'>Testaaaaaaaaaa</a>", ENT_COMPAT,'ISO-8859-1', true);
    $nuevo = htmlentities("<h1>helklo");
    echo $nuevo; // &lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;*/

 
    if($titulo != "error" && $noticia != "error" && $email != "error")
        $status = 1;
         
       // Subir fichero
      $copiarFichero = false;
 
   // Copiar fichero en directorio de ficheros subidos
   // Se renombra para evitar que sobreescriba un fichero existente
   // Para garantizar la unicidad del nombre se añade una marca de tiempo
      if (is_uploaded_file ($_FILES['imagen']['tmp_name']))
      {
         $nombreDirectorio = "images/";
         $nombreFichero = $_FILES['imagen']['name'];
         $copiarFichero = true;
 
      // Si ya existe un fichero con el mismo nombre, renombrarlo
         $nombreCompleto = $nombreDirectorio . $nombreFichero;
         if (is_file($nombreCompleto))
         {
            $idUnico = time();
            $nombreFichero = $idUnico . "-" . $nombreFichero;
         }
      }
   // El fichero introducido supera el límite de tamaño permitido
      else if ($_FILES['imagen']['error'] == UPLOAD_ERR_FORM_SIZE)
      {
         $maxsize = $_REQUEST['MAX_FILE_SIZE'];
         $errores["imagen"] = "¡El tamaño del fichero supera el límite permitido ($maxsize bytes)!";
         $error = true;
      }
   // No se ha introducido ningún fichero
      else if ($_FILES['imagen']['name'] == "")
         $nombreFichero = '';
   // El fichero introducido no se ha podido subir
      else
      {
         $errores["imagen"] = "¡No se ha podido subir el fichero!";
         $error = true;
      }
    
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php require "head.php"; ?>
<title>NOTICIAS | FOCCADI - ONGD</title>
</head>
<!-- posicionamos el foco en el formulario -->
<body onload="formulario.titulo.focus();">
<nav class="navbar navbar-default modal_estilo_nav">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><img src="../images/principales/logo_FOCCADI_texto.png" class="logo_FOCCADI_texto"></a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-user texto_blanco margin_right_10" aria-hidden="true"></i>
              <?php print $_SESSION['name'];?> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                  <a href="cerrar_sesion.php" class="cerrar_sesion"><i class="fa fa-times-circle texto_blanco margin_right_10" aria-hidden="true"></i>Salir</a>
                </li>
            </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  <div class="row well-lg fondo_general_blanco">
    <div class="col-sm-4 col-xs-4">
      <h2 class="texto_verde_1 margin_top_0 margin_bottom_0">Nuevo</h2>
    </div>    
    <div class="col-sm-8 col-xs-8 text-right">      
      <button type="button" class="btn btn-primary icono_noticia_adm" onclick="window.location.href='inicio.php'">
        <i class="fa fa-home texto_blanco" aria-hidden="true"></i><span class="margin_left_10">Inicio</span>
      </button>
      <button type="button" class="btn btn-info icono_noticia_adm" onclick="window.location.href='ingresar_noticia.php'">
        <i class="fa fa-newspaper-o texto_blanco" aria-hidden="true"></i><span class="margin_left_10">Ingresar Noticias</span>
      </button>
      <button type="button" class="btn btn-warning icono_noticia_adm" onclick="window.location.href='eliminar_noticia.php'">
        <i class="fa fa-trash texto_blanco" aria-hidden="true"></i><span class="margin_left_10">Eliminar Noticias</span>
      </button>  
    </div>
  </div>
  <hr class="margin_top_0 margin_bottom_0">
  <div class="row well-lg fondo_general_blanco">
    <div class="col-sm-12">

<?php
    if (isset($insertar) && $error==false) {
       
        $fecha = date ("Y-m-d");
        $instruccion = "INSERT INTO noticias (titulo, descripcion, nombre_categoria, fecha_post_ini, estado, mail, leido, imagen, url) VALUES ('$tituloValue', '$noticiaValue', '$categoria', '$fecha', '1', '$emailValue', '1', '$nombreFichero', '$urlnoticiaValue')";
        $consulta = mysqli_query($conexion,$instruccion) or die ("Fallo en la consulta");

        mysqli_close($conexion);

    // Mover fichero de imagen a su ubicación definitiva
        if ($copiarFichero) {
          move_uploaded_file ($_FILES['imagen']['tmp_name'], $nombreDirectorio . $nombreFichero);
        }
    
    // Mostrar datos introducidos      
        print ("<H2 class='info'>Noticia Insertada con exito</H2>");
        print ("<UL>");
        print ("<LI>Título: " . $titulo);
        print ("<LI>Texto: " . $noticia);
        print ("<LI>Categoría: " . $categoria);
        print ("<LI>Email: " . $email);
   
      if ($nombreFichero != "") {
        print ("<LI>Imagen: <A TARGET='_blank' HREF='" . $nombreDirectorio . $nombreFichero . "'>" . $nombreFichero . "</A>");
      } else {
        print ("<LI>Imagen: (no subió imagen)");
        print ("</UL>");
        print ("<BR>");
        print ("[ <A HREF='ingresar_noticia.php'>Insertar otra noticia</A> ]");
        print ("[ <A HREF='eliminar_noticia.php'>Eliminar noticia</A> ]");
      }

   } else {
?>
 
 
          <form  id="formulario" action="" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="titulo">Ingrese el título de la noticia <span id="req-titulo" class="requisites <?php echo $titulo ?>">(Mínimo 8 caracteres)</span></label>
              <input tabindex="1" name="titulo" id="titulo" type="text" class="form-control <?php echo $titulo ?>" value="<?php echo $tituloValue ?>">
            </div>

            <div class="form-group">
              <label for="noticia">Ingrese Noticia <span id="req-noticia" class="requisites <?php echo $noticia ?>">Mínimo 40 caracteres no caracteres)</span></label>
              <textarea tabindex="2" name="noticia" id="noticia" class="form-control" value="<?php echo $noticiaValue ?>" rows="4" cols="150"></textarea>
            </div>

            <div class="form-group">
              <label for="email">E-mail <span id="req-email" class="requisites <?php echo $email ?>">(Ingrese un email válido)</span></label>
              <input tabindex="3" name="email" id="email" type="text" class="form-control <?php echo $email ?>" value="<?php echo $emailValue ?>" />
            </div>

            <div class="form-group">
              <label for="urlnoticia">Ingrese el Url de la noticia <span class="requisites">(Que incluya http://)</span></label>
              <input tabindex="4" name="urlnoticia" id="urlnoticia" type="text" class="form-control" value="http://" />
            </div>

            <div class="form-group">
              <label for="urlnoticia">Ingrese imagen de la noticia max. 2 MB:</label>
              <input tabindex="5"  TYPE="HIDDEN" NAME="MAX_FILE_SIZE" VALUE="1024000">
              <input type="file" SIZE="44" NAME="imagen" class="form-control">
            </div>
            

            <div class="form-group">
              <label for="categoria">Categoría:</label>
              <select name="categoria" class="form-control">
                <option selected>Noticias</option>
                <option>Proyectos</option>
                <option>Conferencias</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success" name="insertar" id="insertar" value="Enviar Noticia">
              <i class="fa fa-check-circle texto_blanco margin_right_10" aria-hidden="true"></i>Enviar Noticia
            </button>
          </form>

<?php
   }
?>

      <hr class="margin_bottom_0">
      <p class="well-sm margin_bottom_0 text-center">
        <button type="button" class="btn btn-primary btn-sm" onclick="window.location.href='inicio.php'">
          <i class="fa fa-home texto_blanco margin_right_10" aria-hidden="true"></i>Inicio
        </button>
        <button type="button" class="btn btn-info btn-sm" onclick="window.location.href='ingresar_noticia.php'">
          <i class="fa fa-commenting texto_blanco margin_right_10" aria-hidden="true"></i>Ingresar Noticias
        </button>
        <button type="button" class="btn btn-warning btn-sm" onclick="window.location.href='eliminar_noticia.php'">
          <i class="fa fa-trash texto_blanco margin_right_10" aria-hidden="true"></i>Eliminar Noticias
        </button>
      </p>
      <hr class="margin_top_0">
    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery-2.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.js"></script> 
<!-- funciones básicas js -->
<script src="main.js"></script>
<script type="text/javascript">
// Función que cierra la pagina en inactividad
  var idleTime = 0;
  $(document).ready(function(){
    var idleInterval = setInterval(timerIncrement, 1000); // segundo
    $(this).mousemove(function(e){
        idleTime = 0;
    });
    $(this).keypress(function(e){
        idleTime = 0;
    });
  });
  function timerIncrement() {
      idleTime = idleTime + 1;
      if (idleTime < 1200) { // 60 segundos = 1 minuto
        //$("#texto2").text("movimiento");        
      } else {
        window.location.replace("cerrar_sesion.php");
        //$("#texto2").text("detenido");
      }
  }
  timerIncrement();
</script>
</body>
</html>

<?php
//si no hay sesión
} else {
    //se redirecciona
    header ('location: index.php');
}
?>