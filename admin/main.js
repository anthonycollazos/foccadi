$(document).ready(function(){

    //variables globales
    var searchBoxes = $(".text");
    var inputTitulo = $("#titulo");
    var reqTitulo = $("#req-titulo");
    var inputNoticia = $("#noticia");
    var reqNoticia = $("#req-noticia");
    var inputEmail = $("#email");
    var reqEmail = $("#req-email");
 
    //funciones de validacion
    function validateTitulo(){
        //NO cumple longitud minima
        if(inputTitulo.val().length @,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i)){
            reqEmail.addClass("error");
            inputEmail.addClass("error");
            return false;
        }
        // SI rellenado, SI email valido
        else{
            reqEmail.removeClass("error");
            inputEmail.removeClass("error");
            return true;
        }
    }     
     
    //controlamos la validacion en los distintos eventos
    // Perdida de foco
    inputTitulo.blur(validateTitulo);
    inputNoticia.blur(validateNoticia);  
    inputEmail.blur(validateEmail);  
     
    // Pulsacion de tecla
    inputTitulo.keyup(validateTitulo);
    inputNoticia.keyup(validateNoticia);
     
    // Envio de formulario
    $("#formulario").submit(function(){
        if(validateTitulo() & validateNoticia() & validateEmail())
            return true;
        else
            return false;
    });
     
    //controlamos el foco / perdida de foco para los input text
    searchBoxes.focus(function(){
        $(this).addClass("active");
    });
    searchBoxes.blur(function(){
        $(this).removeClass("active");  
    });
 
});