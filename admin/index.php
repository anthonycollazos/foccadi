<?php
session_start();
if (isset($_SESSION['name'])){
  header ('location: inicio.php');
} else {
?>

<!DOCTYPE html>
<html lang="es">
<head>
<?php require "head.php"; ?>
<title>Login NOTICIAS | FOCCADI - ONGD</title>
</head>
<body>

<div class="login_fondo"></div>

<nav class="navbar navbar-default modal_estilo_nav">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><img src="../images/principales/logo_FOCCADI_texto.png" class="logo_FOCCADI_texto"></a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="../index.php">Volver al inicio</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="modal-dialog modal-sm modal_estilo_general margin_top_60">
  <div class="modal-content">
    <div class="modal-header">
      <h3 class="texto_verde_1"><strong>Iniciar sesión</strong></h3>
    </div>
    <div class="modal-body">
      <form action="archivo_destino.php" method="post" enctype="application/x-www-form-urlencoded">
        <div class="form-group">
          <label for="control1_nombre">Usuario</label>
          <input type="text" name="datos_introducidos_usuario" class="form-control" id="control1_nombre" placeholder="Username" required>
        </div>
        <div class="form-group">
          <label for="control1_contraseña">Contraseña</label>
          <input type="password" name="datos_introducidos_contraseña" class="form-control" id="control1_contraseña" placeholder="Password" required>
        </div>
        <button type="submit" class="btn btn-success btn-block fondo_general_verde_2">Entrar</button>
      </form>
    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery-2.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.js"></script> 
</body>
</html>

<?php
}
?>