<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>FOCCADI - ONGD</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- estilos generales -->
<link href="css/estilos.css" rel="stylesheet">
<!-- favicon principal -->
<link rel="shortcut icon" href="images/iconos/favicon.ico">
<!-- font-awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- camera-wrap -->
<link href="plugins/camera/camera.css" rel="stylesheet" type="text/css">
<!-- slick slider -->
<link href="plugins/slick/slick.css" rel="stylesheet" type="text/css">
<!--datepicker-->
<link rel="stylesheet" href="plugins/datepicker/bootstrap-datepicker.min.css">

</head>
<body>

<?php
  include 'includes/menu_top.php';
?>






  <div class="container">
    <div id="corrector_altura"></div>
    <div class="row well-lg">
      <div class="col-sm-12 text-center">
        <h2 class="texto_verde_2"><strong>CONTÁCTANOS</strong></h2>
        <br>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row fondo_general_blanco padding_bottom_0">
      <div class="col-sm-6 well-lg">

        <div class="row">
          <div class="col-sm-12">
            <div class="padding_left_15 padding_right_15">
             
                    <h3 class="texto_verde_1 well-sm">Escríbenos</h3>
                    <p class="well-sm">Así te daremos más información.</p>

                    <form method="POST" action="enviar_mensaje.php" id="form-contact" class="well-sm">
                      <div class="form-group">
                        <label for="nombre">Ingresa tu Nombre:</label>
                        <input type="text" name="nombre" id="nombre" data-email="required" placeholder="Nombre" class="form-control" required>
                      </div>
                      <div class="form-group">
                        <label for="email">Ingresa tu Correo:</label>
                        <input type="text" name="email" id="email" data-email="required" placeholder="Correo" class="form-control" required>
                      </div>
                      <div class="form-group">
                        <label for="fecha">Ingresa una Fecha:</label>                              
                        <input type="" name="fecha" id="fecha" data-email="required" placeholder="dd/mm/aaaa" class="form-control datepicker" required>
                      </div>                              
                      <button type="submit" class="btn btn-foccadi btn-sm"><i class="fa fa-paper-plane texto_blanco margin_right_10" aria-hidden="true"></i><strong>Enviar...</strong></button>
                    </form>

            </div>
          </div>
        </div>

      </div>
      <div class="col-sm-6 padding_right_0 padding_left_0">
        <div class="contacto_img" style="background-image: url(images/interiores/001_FOCCADI_contactanos.jpg);"></div>
      </div>
    </div>
    <div class="row well-lg fondo_general_blanco padding_bottom_0">
      <div class="col-sm-12">
        <hr><br>     
        <h3 class="texto_verde_1 well-sm">Datos de Contacto</h3>
      </div>
    </div>
    <div class="row well-lg fondo_general_blanco padding_bottom_0">
      <div class="col-sm-3 text-center">
        <i class="fa fa-map-marker texto_verde_1 contacto_texto" aria-hidden="true"></i>
        <div class="well-sm">
          <h4 class="texto_verde_2">DIRECCIÓN:</h4>
          <p>Av. Gral. Garzón N° 1082 - Of. 302, Jesús María – Lima - Perú</p>
        </div>
      </div>
      <div class="col-sm-3 text-center">
        <i class="fa fa-phone texto_verde_1 contacto_texto" aria-hidden="true"></i>
        <div class="well-sm">          
          <h4 class="texto_verde_2">TELÉFONOS:</h4>
          <p>(511) 423-8305<br>Cel: 998523739</p>
        </div>
      </div>
      <div class="col-sm-3 text-center">
        <i class="fa fa-envelope-o texto_verde_1 contacto_texto" aria-hidden="true"></i>
        <div class="well-sm">
          <h4 class="texto_verde_2">E-MAIL:</h4>
          <p>administración@foccadi.org.pe</p>
        </div>
      </div>
      <div class="col-sm-3 text-center">
        <i class="fa fa-globe texto_verde_1 contacto_texto" aria-hidden="true"></i>
        <div class="well-sm">
          <h4 class="texto_verde_2">WEBSITE:</h4>
          <p>www.foccadi.org.pe</p>
        </div>
      </div>
    </div>
    <div class="row well-lg fondo_general_blanco padding_bottom_0">
      <div class="col-sm-12">
        <hr><br>
        <h3 class="texto_verde_1 well-sm">Mapa</h3>
        <iframe height="600" allowfullscreen="" style="border: 0; width: 100%; vertical-align: bottom; display: block; pointer-events: none;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.595539129437!2d-77.0494943445081!3d-12.071325641556502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c8e4377f7ff9%3A0x8e034099dbd12926!2sGeneral+Garz%C3%B3n+1082%2C+Jes%C3%BAs+Mar%C3%ADa+15072!5e0!3m2!1ses!2spe!4v1509097186913" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        <br><br>
      </div>
    </div>
  </div>


<br><br><br>

<?php
  include 'includes/footer.php';
?>




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<!-- <script src="js/jquery-1.11.2.min.js"></script> -->
<script src="js/jquery-2.1.1.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.js"></script>

<!-- Galería Camera Wrap -->
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="plugins/camera/camera.min.js"></script>

<!-- Galería Slick Slider -->
<script type="text/javascript" src="plugins/slick/slick.min.js"></script>

<!-- Plugin Datepicker -->
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.min.js"></script>
<script>
  $('.datepicker').datepicker();
</script>

<!-- Plugin Facebook --> 
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10&appId=1057542310947424';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
  $(document).ready(function(){
    
    $("#boton_menu").click(function() {
      if ($("#navegador_bottom_2").is(":visible")) {
        $("#navegador_bottom_2").slideUp("slow");
      } else {
        $("#navegador_bottom_2").slideDown("slow");
      }
    });

    $(window).scroll(function() {
      if ($(document).scrollTop() > 0) {
        $("#navegador_principal").addClass("activado");
        $("#corrector_altura").addClass("activado");
        $("#navegador_1").addClass("activado");
        $(".fondo_verde_2").addClass("activado");
        $("#contenedor_logo").addClass("activado");
        $(".logo_principal").addClass("activado");
        $(".menu_siguenos").addClass("activado"); 
        $("#navegador_top li a").addClass("activado");
        $("#navegador_bottom").addClass("activado"); 
        $("#navegador_bottom li .enlace_bottom").addClass("activado"); 
        $("img.icono_donar").addClass("activado"); 
      } else {
        $("#navegador_principal").removeClass("activado");
        $("#corrector_altura").removeClass("activado");
        $("#navegador_1").removeClass("activado");
        $(".fondo_verde_2").removeClass("activado");
        $("#contenedor_logo").removeClass("activado");
        $(".logo_principal").removeClass("activado");
        $(".menu_siguenos").removeClass("activado");   
        $("#navegador_top li a").removeClass("activado");
        $("#navegador_bottom").removeClass("activado");
        $("#navegador_bottom li .enlace_bottom").removeClass("activado"); 
        $("img.icono_donar").removeClass("activado"); 
      }
    });

  });
</script>




</body>
</html>