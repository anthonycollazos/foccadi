-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 21-11-2017 a las 22:42:03
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_noticias`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

DROP TABLE IF EXISTS `noticias`;
CREATE TABLE IF NOT EXISTS `noticias` (
  `noticia_id` int(10) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `nombre_categoria` enum('Noticias','Proyectos','Conferencias') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Noticias',
  `fecha_post_ini` date NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(40) CHARACTER SET utf8 NOT NULL,
  `leido` int(10) NOT NULL DEFAULT '1',
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`noticia_id`)
) ENGINE=MyISAM AUTO_INCREMENT=413566706 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`noticia_id`, `titulo`, `descripcion`, `nombre_categoria`, `fecha_post_ini`, `estado`, `mail`, `leido`, `imagen`, `url`) VALUES
(413566702, 'â€œDifusiÃ³nâ€ - Presidente de Foccadi en Simposio â€“ MIMP', 'â€œCada etapa de esta compleja enfermedad requiere de una atenciÃ³n especial a cargo de tÃ©cnicos calificadosâ€, manifestÃ³ Edwin Delgado Baca, presidente de la ONGD Fortalecimiento Creativo de Capacidades Distintivas (FOCCADI), cuando visitÃ³ el programa â€œA los 60â€ para explicar cÃ³mo trabajan para mejorar la calidad de vida de los adultos mayores con Alzheimer. Al respecto, Delgado recordÃ³ que â€œEste tipo de demencia es una enfermedad que requiere de personas calificadas. Los especialistas formados en nuestra instituciÃ³n estÃ¡n preparados para orientar a los familiares sobre cÃ³mo deben cuidar a las personas con Alzheimerâ€, aÃ±adiÃ³. ', 'Noticias', '2017-11-21', '1', 'anthony@hotmail.com', 1, '1511303080-005_FOCCADI_noticias.jpg', 'http://'),
(413566703, 'â€œDifusiÃ³nâ€ - Presidente de Foccadi en Simposio â€“ MIMP', 'â€œCada etapa de esta compleja enfermedad requiere de una atenciÃ³n especial a cargo de tÃ©cnicos calificadosâ€, manifestÃ³ Edwin Delgado Baca, presidente de la ONGD Fortalecimiento Creativo de Capacidades Distintivas (FOCCADI), cuando visitÃ³ el programa â€œA los 60â€ para explicar cÃ³mo trabajan para mejorar la calidad de vida de los adultos mayores con Alzheimer. Al respecto, Delgado recordÃ³ que â€œEste tipo de demencia es una enfermedad que requiere de personas calificadas. Los especialistas formados en nuestra instituciÃ³n estÃ¡n preparados para orientar a los familiares sobre cÃ³mo deben cuidar a las personas con Alzheimerâ€, aÃ±adiÃ³. ', 'Noticias', '2017-11-21', '1', 'anthony@hotmail.com', 1, '1511303096-005_FOCCADI_noticias.jpg', 'http://'),
(413566694, 'â€œDifusiÃ³nâ€ - Presidente de Foccadi en Simposio â€“ MIMP ', 'â€œCada etapa de esta compleja enfermedad requiere de una atenciÃ³n especial a cargo de tÃ©cnicos calificadosâ€, manifestÃ³ Edwin Delgado Baca, presidente de la ONGD Fortalecimiento Creativo de Capacidades Distintivas (FOCCADI), cuando visitÃ³ el programa â€œA los 60â€ para explicar cÃ³mo trabajan para mejorar la calidad de vida de los adultos mayores con Alzheimer. Al respecto, Delgado recordÃ³ que â€œEste tipo de demencia  es una enfermedad que requiere de personas calificadas. Los especialistas formados en nuestra instituciÃ³n estÃ¡n preparados para orientar a los familiares sobre cÃ³mo deben cuidar a las personas con Alzheimerâ€, aÃ±adiÃ³.\r\n                ', 'Noticias', '2017-10-26', '1', 'anthonycollazos@hotmail.com', 1, '005_FOCCADI_noticias.jpg', 'http://anthony.com.pe'),
(413566699, 'â€œOpiniÃ³n Favorableâ€ - Ministerio de Defensa', 'â€œCada etapa de esta compleja enfermedad requiere de una atenciÃ³n especial a cargo de tÃ©cnicos calificadosâ€, manifestÃ³ Edwin Delgado Baca, presidente de la ONGD Fortalecimiento Creativo de Capacidades Distintivas (FOCCADI), cuando visitÃ³ el programa â€œA los 60â€ para explicar cÃ³mo trabajan para mejorar la calidad de vida de los adultos mayores con Alzheimer. Al respecto, Delgado recordÃ³ que â€œEste tipo de demencia es una enfermedad que requiere de personas calificadas. Los especialistas formados en nuestra instituciÃ³n estÃ¡n preparados para orientar a los familiares sobre cÃ³mo deben cuidar a las personas con Alzheimerâ€, aÃ±adiÃ³. ', 'Noticias', '2017-11-21', '1', 'anthony@hotmail.com', 1, '1511302822-001_FOCCADI_noticias.jpg', 'http://'),
(413566700, 'â€œFelicitacionesâ€ FundaciÃ³n Carlos Slim de Mexico', 'â€œCada etapa de esta compleja enfermedad requiere de una atenciÃ³n especial a cargo de tÃ©cnicos calificadosâ€, manifestÃ³ Edwin Delgado Baca, presidente de la ONGD Fortalecimiento Creativo de Capacidades Distintivas (FOCCADI), cuando visitÃ³ el programa â€œA los 60â€ para explicar cÃ³mo trabajan para mejorar la calidad de vida de los adultos mayores con Alzheimer. Al respecto, Delgado recordÃ³ que â€œEste tipo de demencia es una enfermedad que requiere de personas calificadas. Los especialistas formados en nuestra instituciÃ³n estÃ¡n preparados para orientar a los familiares sobre cÃ³mo deben cuidar a las personas con Alzheimerâ€, aÃ±adiÃ³. ', 'Noticias', '2017-11-21', '1', 'anthony@hotmail.com', 1, '1511303008-003_FOCCADI_noticias.jpg', 'http://'),
(413566701, 'â€œDifusiÃ³nâ€ - Director MÃ©dico de Foccadi', 'â€œCada etapa de esta compleja enfermedad requiere de una atenciÃ³n especial a cargo de tÃ©cnicos calificadosâ€, manifestÃ³ Edwin Delgado Baca, presidente de la ONGD Fortalecimiento Creativo de Capacidades Distintivas (FOCCADI), cuando visitÃ³ el programa â€œA los 60â€ para explicar cÃ³mo trabajan para mejorar la calidad de vida de los adultos mayores con Alzheimer. Al respecto, Delgado recordÃ³ que â€œEste tipo de demencia es una enfermedad que requiere de personas calificadas. Los especialistas formados en nuestra instituciÃ³n estÃ¡n preparados para orientar a los familiares sobre cÃ³mo deben cuidar a las personas con Alzheimerâ€, aÃ±adiÃ³. ', 'Noticias', '2017-11-21', '1', 'anthony@hotmail.com', 1, '1511303031-004_FOCCADI_noticias.jpg', 'http://'),
(413566704, 'â€œDifusiÃ³nâ€ - Presidente de Foccadi en Simposio â€“ MIMP', 'â€œCada etapa de esta compleja enfermedad requiere de una atenciÃ³n especial a cargo de tÃ©cnicos calificadosâ€, manifestÃ³ Edwin Delgado Baca, presidente de la ONGD Fortalecimiento Creativo de Capacidades Distintivas (FOCCADI), cuando visitÃ³ el programa â€œA los 60â€ para explicar cÃ³mo trabajan para mejorar la calidad de vida de los adultos mayores con Alzheimer. Al respecto, Delgado recordÃ³ que â€œEste tipo de demencia es una enfermedad que requiere de personas calificadas. Los especialistas formados en nuestra instituciÃ³n estÃ¡n preparados para orientar a los familiares sobre cÃ³mo deben cuidar a las personas con Alzheimerâ€, aÃ±adiÃ³. ...', 'Proyectos', '2017-11-21', '1', 'anthony@hotmail.com', 1, '1511303689-004_FOCCADI_noticias.jpg', 'http://');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `identificador` int(11) NOT NULL AUTO_INCREMENT,
  `columna_usuario` varchar(255) NOT NULL,
  `columna_password` varchar(255) NOT NULL,
  PRIMARY KEY (`identificador`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`identificador`, `columna_usuario`, `columna_password`) VALUES
(1, 'holaprueba', '12345678'),
(2, 'holaquetal', '$2y$10$yxDEEoMEeFXp9vywNSQ7kuYzUT0Dv0NQFdj2vs28zxXAxdmvOVgau'),
(6, 'adminfoccadi', '$2y$10$TWXY5zTSXR2cQteP6zlO6uhlYtzeeQEhwFOgfGgEn3LhrzKZP8K2q'),
(7, 'anthony', '$2y$10$rEXy/l2tJNxnWg8pUG5fCuCXXN1f4Nm7dQ/fKcT.9YKPeDFFdwchi');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
