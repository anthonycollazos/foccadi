

<nav id="navegador_principal">
  <div id="navegador_1">
    <div class="fondo_blanco"></div>
    <div class="fondo_verde_1">
      <div class="fondo_verde_2"></div>
    </div>
  </div>
  <div id="navegador_2" class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="fondo_navegador">
          <div id="contenedor_logo">
            <a href="index.php"><img src="images/principales/logo_FOCCADI.png" class="logo_principal"></a>
          </div>
          <div class="fondo_verde_2">            
            <ul id="navegador_top" class="nav navbar-nav">
              <li><a class="enlace_top texto_blanco" href="index.php"><i class="fa fa-home texto_blanco margin_right_10" aria-hidden="true"></i>Inicio</a></li>
              <li><a class="enlace_top texto_blanco" href="nosotros.php"><i class="fa fa-users texto_blanco margin_right_10" aria-hidden="true"></i>Nosotros</a></li>
              <!-- <li><a class="enlace_top texto_blanco" href="noticias.php">• Noticias</a></li>
              <li><a class="enlace_top texto_blanco" href="proyectos.php">• Proyectos</a></li>
              <li><a class="enlace_top texto_blanco" href="conferencias.php">• Conferencias</a></li> -->
              <li><a class="enlace_top texto_blanco" href="contactanos.php"><i class="fa fa-address-book texto_blanco margin_right_10" aria-hidden="true"></i> Contáctanos</a></li>
              <li>
                <p class="texto_blanco menu_siguenos"><span class="margin_right_15 margin_left_30">Síguenos</span>
                  <a class="enlace_top padding_left_15 border_left_blanco" href="#"><i class="fa fa-facebook texto_blanco" aria-hidden="true"></i></a>
                  <a class="enlace_top margin_left_10" href="#"><i class="fa fa-twitter texto_blanco" aria-hidden="true"></i></a>
                  <a class="enlace_top margin_left_10" href="#"><i class="fa fa-youtube-play texto_blanco" aria-hidden="true"></i></a>                    
                </p>
              </li>
            </ul>
            <ul id="navegador_top_2" class="">
              <li><p class="texto_blanco margin_right_15 menu_siguenos">Síguenos</p></li>
              <li>
                <p class="texto_blanco menu_siguenos">
                  <a class="enlace_top padding_left_15 border_left_blanco" href="#"><i class="fa fa-facebook texto_blanco" aria-hidden="true"></i></a>
                  <a class="enlace_top margin_left_10" href="#"><i class="fa fa-twitter texto_blanco" aria-hidden="true"></i></a>
                  <a class="enlace_top margin_left_10" href="#"><i class="fa fa-youtube-play texto_blanco" aria-hidden="true"></i></a>                    
                </p>
              </li>
            </ul>
          </div>
          <ul id="navegador_bottom" class="nav navbar-nav">
            <!-- <li><a class="enlace_bottom texto_blanco" href="index.php#quienes_somos">¿QUIÉNES SOMOS?</a></li>
            <li><a class="enlace_bottom texto_blanco" href="index.php#que_hacemos">¿QUÉ HACEMOS?</a></li>
            <li><a class="enlace_bottom texto_blanco" href="index.php#que_hacemos">¿QUÉ NOS IDENTIFICA?</a></li>
            <li><a class="enlace_bottom texto_blanco" href="index.php#que_hacemos">¿QUÉ PUEDES HACER TÚ?</a></li> -->
            <li><a class="enlace_bottom texto_blanco" href="nosotros.php">NOSOTROS</a></li>
            <li><a class="enlace_bottom texto_blanco" href="noticias.php">NOTICIAS</a></li>
            <li><a class="enlace_bottom texto_blanco" href="proyectos.php">PROYECTOS</a></li>
            <li><a class="enlace_bottom texto_blanco" href="conferencias.php">CONFERENCIAS</a></li>
            <li><a class="enlace_bottom texto_blanco enlace_donar" href="donar.php"><div class="boton_donar padding_right_15"><img src="images/iconos/icono_04b_FOCCADI.png" class="img-responsive icono_donar margin_left_15 margin_right_10" alt="">DONAR</div></li>
          </ul>
          <a id="boton_menu" class="texto_blanco" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
          <ul id="navegador_bottom_2" class="">
            <li><a class="enlace_bottom_2 texto_blanco" href="index.php">• Inicio</a></li>
            <li><a class="enlace_bottom_2 texto_blanco" href="nosotros.php">• Nosotros</a></li>
            <li><a class="enlace_bottom_2 texto_blanco" href="noticias.php">• Noticias</a></li>
            <li><a class="enlace_bottom_2 texto_blanco" href="proyectos.php">• Proyectos</a></li>
            <li><a class="enlace_bottom_2 texto_blanco" href="conferencias.php">• Conferencias</a></li>
            <li><a class="enlace_bottom_2 texto_blanco" href="contactanos.php">• Contáctanos</a></li>
            <li><a class="enlace_bottom_2 texto_blanco" href="donar.php">• Donar</a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</nav>