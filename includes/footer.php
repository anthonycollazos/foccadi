<footer id="footer_principal" class="">
  <div class="container">
    <div class="row well-lg padding_bottom_0">
      <div class="col-sm-3">
        <ul class="well-sm texto_blanco margin_bottom_0">
          <li><a class="enlace_footer texto_blanco" href="index.php"><i class="fa fa-home texto_blanco margin_right_10" aria-hidden="true"></i>Inicio</a></li>
          <li><a class="enlace_footer texto_blanco" href="nosotros.php"><i class="fa fa-users texto_blanco margin_right_10" aria-hidden="true"></i>Nosotros</a></li>
          <li><a class="enlace_footer texto_blanco" href="contactanos.php"><i class="fa fa-address-book texto_blanco margin_right_10" aria-hidden="true"></i> Contáctanos</a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <ul class="well-sm texto_blanco margin_bottom_0">
            <li><a class="enlace_footer texto_blanco" href="noticias.php">NOTICIAS</a></li>
            <li><a class="enlace_footer texto_blanco" href="proyectos.php">PROYECTOS</a></li>
            <li><a class="enlace_footer texto_blanco" href="conferencias.php">CONFERENCIAS</a></li>
            <li><a class="enlace_footer texto_blanco" href="donar.php"><img src="images/iconos/icono_04b_FOCCADI.png" class="img-responsive icono_donar_2 margin_right_10" alt="">DONAR</li>
        </ul>
      </div>
      <div class="col-sm-3">
        <ul class="well-sm texto_blanco margin_bottom_0">
          <li><a class="enlace_footer texto_blanco" href="">Síguenos</a></li>          
          <li>
            <p class="texto_blanco menu_siguenos">
              <a class="enlace_top" href="#"><i class="fa fa-facebook texto_blanco" aria-hidden="true"></i></a>
              <a class="enlace_top margin_left_10" href="#"><i class="fa fa-twitter texto_blanco" aria-hidden="true"></i></a>
              <a class="enlace_top margin_left_10" href="#"><i class="fa fa-youtube-play texto_blanco" aria-hidden="true"></i></a>
            </p>
          </li>
        </ul>
      </div>
      <div class="col-sm-3 text-right">
        <a href="index.php"><img src="images/principales/logo_FOCCADI_blanco.png" class="logo_principal"></a>
      </div>
    </div>
    <div class="row well-lg">
      <hr>
      <div class="col-sm-12 text-center">
        <p class="p_footer texto_blanco">© 2017 diseñado por Nameless.</p>
      </div>
    </div>
  </div>
</footer>